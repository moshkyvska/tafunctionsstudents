using System;

namespace TAFunctions
{
    public enum SortOrder { Ascending, Descending };
    static public class OperationsWithArrays
    {
        /// <summary>
        /// Should sort array in SortOrder order
        /// <exception cref="ArgumentNullException">Thrown when wrong parameter array</exception>
        /// </summary>

        public static void SortArray(int[] array, SortOrder order)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Should to check whether the array is sorted in SortOrder order. An array should be not changed
        /// <exception cref="ArgumentNullException">Thrown when wrong parameter array</exception>
        /// </summary>
        public static bool IsSorted(int[] array, SortOrder order)
        {
            throw new NotImplementedException();
        }
    }
}